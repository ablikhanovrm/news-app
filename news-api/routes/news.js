import express from "express";
import { nanoid } from "nanoid";
import * as path from 'path';
import multer from "multer";
import config from "../config.js";
import News from "../Models/News.js";
import Comment from "../Models/Comment.js";

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
})

const upload = multer({storage})
const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const news = await News.find({}, {title: 1, _id: 1, image: 1, date: 1});
    res.send(news);
  } catch {
    res.sendStatus(500);
  }
 });
 

  router.get('/:id', async (req, res) => {
    try {
      const news = await News.findById(req.params.id);
      res.send(news);
    } catch {
      res.sendStatus(500);
    }
 });


router.post('/', upload.single('image'), async (req, res) => {
  const body = {...req.body, date: new Date().toISOString()};
  if(req.file) {
    body.image = req.file.filename;
  }
  const news = new News(body);
  try {
    await news.save();
    res.send(news);
  } catch(e) {
    res.sendStatus(500);
  }
})


router.delete('/:id', async (req, res) => {
  try {
    console.log(req.params.id);
    const deletedNews = await News.deleteOne({_id: req.params.id});
    await Comment.deleteMany({news: req.params.id});
    res.send({...deletedNews, id: req.params.id});
  } catch (e) {
    res.status(400).send(e);
  }
})


export default router;