import express from "express";
import Comment from "../Models/Comment.js";

const router = express.Router();

router.post('/', async (req, res) => {
    const body = {...req.body, date: new Date().toISOString()};
    const comment = new Comment(body);
    try {
        await comment.save();
        res.send(comment);
    } catch(e) {
        res.sendStatus(500);
    }
})

router.get('/', async (req, res) => {
    let comments = await Comment.find();
    if(req.query.news_id){
        const news = req.query.news_id;
        try {
            comments = await Comment.find({news: news});
           return res.send(comments);
        } catch {
            res.sendStatus(500);
        }
    }
    try {
        return res.send(comments);
    } catch {
        res.sendStatus(500);
    }
 });
 

 
 router.delete('/:id', async (req, res) => {
     try {
         const deletedComment = await Comment.remove({_id: req.params.id})
         res.send({...deletedComment, id: req.params.id});
        } catch (e) {
            res.status(400).send(e);
        }
    })
    

export default router;