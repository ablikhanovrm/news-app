import mongoose from "mongoose";

const Scheme = mongoose.Schema;

const NewsScheme = new Scheme({
    title: {
        type: String,
        required: true,
    },
    discription: {
        type: String,
        required: true,
    },
    image: String,
    date: String,
})

const News = mongoose.model('News', NewsScheme);

export default News;