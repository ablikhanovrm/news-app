import mongoose from "mongoose";

const Scheme = mongoose.Schema;

const CommentScheme = new Scheme({
    news: {
        type: Scheme.Types.ObjectId,
        ref: 'News',
        required: true,
    },
    author: {
        type: String,
        default: "Anonymous"
    },
    comment: {
        type: String,
        required: true,
    },
    date: String,
})

const Comment = mongoose.model('Comment', CommentScheme);

export default Comment;