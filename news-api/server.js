import express from "express";
import mongoose from "mongoose";
import cors from 'cors';
import news from './routes/news.js';
import comments from './routes/comments.js';

const app = express();
const PORT = 8000;

app.use(express.static('public'));
app.use(express.json());
app.use(cors());
app.use('/news', news);
app.use('/comments', comments);


const run  = async() => {
    mongoose.connect('mongodb://localhost/someNews', {useNewUrlParser: true});

    app.listen(PORT, () => {
        console.log(`Server started at http://localhost:${PORT}/`);
    })

    process.on("exit", () => {
        mongoose.disconnect();
    })

}


run().catch(console.error);