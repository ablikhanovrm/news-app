import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./Components/Layout/Layout";
import Homepage from "./Containers/Homepage/Homepage";
import CreatePost from "./Components/CreatePost/CreatePost";
import FullPost from "./Containers/FullPost/FullPost";

function App() {
  return (
    <BrowserRouter>
    <Routes>
      <Route path="/" element={<Layout/>}>
          <Route index element={<Homepage/>}/>
          <Route path="add-news" element={<CreatePost/>}/>
          <Route path="/news/:id" element={<FullPost/>}/>
      </Route>
    </Routes>  
  </BrowserRouter>    
  );
}

export default App;
