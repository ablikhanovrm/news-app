import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import axiosApi from '../axiosApi.js';


const initialState = {
    allNews: [],
    currentNews: {},
    comments: [],
    isLoading: false,
    error: null,
};

export const fetchNews = createAsyncThunk(
    'news/fetch',
    async () => {
        return await axiosApi.get('/news').then(res => res.data);
    }
)

export const fetchCurrentNews = createAsyncThunk(
    'currentNews/fetch',
    async (payload) => {
        return await axiosApi.get(`/news/${payload}`).then(res => res.data);
    }
)

export const createNews = createAsyncThunk(
    'news/create',
    async(payload) => {
        return await axiosApi.post('/news', payload).then(res => res.data);
    }
)

export const deleteNews = createAsyncThunk(
    'news/delete',
    async(payload) => {
        return await axiosApi.delete(`/news/${payload}`).then(res => res.data);
    }
)

export const fetchComments = createAsyncThunk(
    'comments/fetch',
    async (payload) => {
        return await axiosApi.get(`/comments?news_id=${payload}`).then(res => res.data);
    }
)

export const createComments = createAsyncThunk(
    'comment/create',
    async(payload) => {
        return await axiosApi.post('/comments', payload).then(res => res.data);
    }
)

export const deleteComment = createAsyncThunk(
    'comment/delete',
    async(payload) => {
        return await axiosApi.delete(`/comments/${payload}`).then(res => res.data);
    }
)


const newsSlice = createSlice({
    name: 'news',
    initialState,
    extraReducers: builder => {
        builder
        .addCase(fetchNews.pending, state => {
            state.isLoading = true;
        })
        .addCase(fetchNews.rejected, (state, action) => {
            state.error = action.payload
            state.isLoading = false;  
        })
        .addCase(fetchNews.fulfilled, (state, action) => {
            state.isLoading = false;
            state.allNews = action.payload;
        })
        .addCase(fetchCurrentNews.pending, state => {
            state.isLoading = true;
        })
        .addCase(fetchCurrentNews.rejected, (state, action) => {
            state.error = action.payload
            state.isLoading = false;  
        })
        .addCase(fetchCurrentNews.fulfilled, (state, action) => {
            state.currentNews = action.payload;
            state.isLoading = false;
        })
        .addCase(createNews.pending, state => {
            state.isLoading = true;
        })
        .addCase(createNews.rejected, (state, action) => {
            state.error = action.payload
            state.isLoading = false;  
        })
        .addCase(createNews.fulfilled, state => {
            state.isLoading = false;
        })
        .addCase(deleteNews.pending, state => {
            state.isLoading = true;
        })
        .addCase(deleteNews.rejected, (state, action) => {
            state.error = action.payload
            state.isLoading = false;  
        })
        .addCase(deleteNews.fulfilled, (state, action) => {
            const index = state.allNews.findIndex(c => c._id === action.payload.id)
            state.allNews.splice(index, 1);
            state.isLoading = false;
        })
        .addCase(fetchComments.pending, state => {
            state.isLoading = true;
        })
        .addCase(fetchComments.rejected, (state, action) => {
            state.error = action.payload
            state.isLoading = false;  
        })
        .addCase(fetchComments.fulfilled, (state, action) => {
            state.comments = action.payload;
            state.isLoading = false;
        })
        .addCase(deleteComment.pending, state => {
            state.isLoading = true;
        })
        .addCase(deleteComment.rejected, (state, action) => {
            state.error = action.payload
            state.isLoading = false;
        })
        .addCase(deleteComment.fulfilled, (state, action) => {
            const index = state.comments.findIndex(c => c._id === action.payload.id)
            state.comments.splice(index, 1);
            state.isLoading = false;
        })
        .addCase(createComments.pending, state => {
            state.isLoading = true;
        })
        .addCase(createComments.rejected, (state, action) => {
            state.error = action.payload
            state.isLoading = false;  
        })
        .addCase(createComments.fulfilled, (state, action) => {
            state.comments.push(action.payload);
            state.isLoading = false;
        })
    }
})

export default newsSlice.reducer;