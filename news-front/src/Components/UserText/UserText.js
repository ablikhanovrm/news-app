import React from "react";
import './UserText.css';

const UserText = ({textChange, textValue, textPlaceHolder, textName}) => {

    return (
        <textarea 
            name={textName}
            className="userText"
            value={textValue}
            placeholder={textPlaceHolder}
            onChange={textChange}
        >
        </textarea>
    )

}

export default UserText;