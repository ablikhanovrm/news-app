import React from "react";
import './InputTitle.css';

const InputTitle = ({titleChanger, title, textPlaceHolder, inputName}) => {
    
    return (
        <input
            onChange={titleChanger} 
            name={inputName}
            value={title}
            className="inputTitle" 
            placeholder={textPlaceHolder}>
        </input>
    );
}

export default InputTitle;