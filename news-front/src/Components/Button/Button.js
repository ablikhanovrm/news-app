import React from "react";
import './Button.css';

const Button = ({create, text, isDisabled}) => {

    return(
        <button
            disabled={isDisabled}
            onClick={create} 
            className="saveButton glow-on-hover">
            {text}
        </button>
    )

}

export default Button;