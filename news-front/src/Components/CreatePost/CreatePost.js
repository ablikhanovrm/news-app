import React, { useState } from "react";
import Spinner from "../Spinner/Spinner";
import { shallowEqual, useSelector, useDispatch } from "react-redux";
import './CreatePost.css';
import InputTitle from "../InputTitle/InputTitle";
import UserText from "../UserText/UserText";
import Button from "../Button/Button";
import FileInput from "./ImgInput/FileInput";
import { createNews } from "../../Store/newsSlice";
import { useNavigate } from "react-router-dom";

const CreatePost = () => {

    const dispatch = useDispatch();
    const navigate = useNavigate();
    const {isLoading} = useSelector(state => state.news, shallowEqual);

    const [state, setState] = useState({
        title: '',
        discription: '',
        image: ''
    });

    const fileChangeHandler = e => {
        const {name} = e.target;
        const file = e.target.files[0];
        setState(prevState => ({
            ...prevState,
            [name]: file
        }));
    }

    const submitHandler = async (e) => {
        e.preventDefault();
        const formData = new FormData();
        for(let key in state) {
            formData.append(key, state[key]);
        }
        await dispatch(createNews(formData));
        console.log(formData);
        navigate('/')
    }


    const inputChangeHandler = e => {
        const {name, value} = e.target;
        setState(prevState => ({...prevState, [name]: value}));
    }


    return (
        <>
            <div className="createNews">
                <h1  className="AddTitle">Add New Post</h1>
                <form onSubmit={submitHandler}>
                    <InputTitle
                        textPlaceHolder="Введите Ваше имя..."
                        titleChanger={inputChangeHandler}
                        inputName="title"
                        title={state.title}
                    />
                    <UserText
                        textName="discription"
                        textPlaceHolder="Введите содержимое поста..."
                        textChange={inputChangeHandler}
                        textValue={state.discription}
                    />
                    <FileInput
                        fileChangeHandler={fileChangeHandler}
                    />
                    <Button
                        text="Отправить"
                        create={submitHandler}
                    />      
                </form>
            </div>
            {isLoading ? <Spinner/> : null}
        </>
    )
}
export default CreatePost;