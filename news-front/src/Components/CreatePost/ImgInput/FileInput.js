import React from "react";
import './FileInput.css';

const FileInput = ({fileChangeHandler}) => {

    return (
        <input 
            className="fileInput" 
            type="file" 
            title="Upload File" 
            onChange={fileChangeHandler}
            name='image'
        />
    )
}

export default FileInput;