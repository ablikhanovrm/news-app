import React from "react";
import './Comment.css';

const Comment = ({author, text, deleteComment, date}) => {
    return (
        <div className="comment">
            <div className="author">
                <p className="authorName">
                    <strong>Автор: </strong>
                    {author}
                </p>
                <span 
                    className="comment_date">
                    {new Date(date).toLocaleString()}
                </span>
            </div>
            <p className="comment_text">{text}</p>
            <button
                className="deleteButton"
                onClick={deleteComment}
            >Delete
            </button>
        </div>
    )
}

export default Comment;