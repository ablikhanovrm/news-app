import React from "react";
import Comment  from './Comment/Comment';
import './Comments.css';

const Comments = ({comments, deletehandler}) => {
    return (
        <div className="comments">
              <h1>Comments</h1>
        {comments.map(com => {
            return (
                <Comment
                    key={com._id}
                    date={com.date}
                    author={com.author}
                    text={com.comment}
                    deleteComment={() => {deletehandler(com._id)}}
                />
            )
        })}
    </div>
    )
}

export default Comments;