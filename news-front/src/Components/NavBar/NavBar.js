import React from "react";
import { NavLink } from "react-router-dom";
import './NavBar.css';

const NavBar = () => {
    return (
        <header className="header">
            <nav className="navBar">
                <NavLink className="navLink LinkNews" to="/">News</NavLink>
                <NavLink className="navLink LinkAdd" to="add-news">Add new post</NavLink>
            </nav>
        </header>
    )
}

export default NavBar;