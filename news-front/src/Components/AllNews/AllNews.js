import React from "react";
import './AllNews.css';
import Post from "./Post/Post";

const AllNews = ({posts, fullHandler, deletehandler}) => {
    return (
        <div className="allNews">
            {posts.map(post => {
                return (
                    <Post
                        key={post._id}
                        image={post.image}
                        postTime={post.date}
                        postTitle={post.title}
                        deltePost={() => {deletehandler(post._id)}}
                        fullPost={() => {fullHandler(post._id)}}
                    />
                )
            })}
        </div>
    )
}

export default AllNews;