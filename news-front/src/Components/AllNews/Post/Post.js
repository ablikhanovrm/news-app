import React from "react";
import './Post.css';
import apiURL from "../../../config";
import noAvailable from '../../../assets/images/noAvailable.jpeg';


const Post = ({postTitle, image, postTime, fullPost, deltePost}) => {
    
    const time = new Date(postTime).toLocaleString();
    let postImage = noAvailable;
    
    if(image) postImage = `${apiURL}/uploads/${image}`;

    return  (
        <div className="post">
            <img className="postImg" src={postImage} alt="" />
                <div className="postBody">
                    <span className="postTime">{time}</span>
                    <h4 className="postTitle">{postTitle}</h4>
                        <div className="post_control">
                            <button
                                onClick={fullPost}
                                className="fullPostButton"
                            >
                                Read Full Post{`>>`}
                            </button>
                            <button
                                onClick={deltePost}
                                className="deletePost"
                            >
                                Delete
                            </button>
                        </div>
                
                </div>
        </div>
    );
}

export default Post;