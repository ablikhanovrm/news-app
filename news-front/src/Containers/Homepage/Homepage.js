import React, {useEffect} from "react";
import AllNews from "../../Components/AllNews/AllNews";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { fetchNews, deleteNews } from "../../Store/newsSlice";
import Spinner from "../../Components/Spinner/Spinner";
import { useNavigate } from "react-router-dom";
import './Homepage.css';

const Homepage = () => {
    const {allNews, isLoading} = useSelector(state => state.news, shallowEqual)
    const dispatch = useDispatch();
    const navigate = useNavigate();

    useEffect(() => {
        dispatch(fetchNews());
    }, [dispatch])


    const deletePostHandler = (id) => {
        dispatch(deleteNews(id))
    }
 
    const fullPostHandler = (id) => {
        navigate(`/news/${id}`);
    }

    return (
        <>
            <div className="main">
                <h1 className="homePageTitle" >Posts</h1>
                {allNews.length > 0 ? 
                <AllNews
                    fullHandler={fullPostHandler}
                    deletehandler={deletePostHandler}
                    posts={allNews}
                />
                : 
                <div className="noPost">
                    <h1 className="noPostTitle">No Posts...</h1>
                </div>
                }
            </div>
            {isLoading ? <Spinner/> : null}
        </>
    )
} 

export default Homepage;