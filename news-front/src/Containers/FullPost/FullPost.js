import React from "react";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { fetchCurrentNews, fetchComments, deleteComment, createComments } from '../../Store/newsSlice.js'
import noAvailable from '../../assets/images/noAvailable.jpeg';
import Comments from "../../Components/Comments/Comments.js";
import InputTitle from "../../Components/InputTitle/InputTitle.js";
import UserText from "../../Components/UserText/UserText.js";
import Spinner from "../../Components/Spinner/Spinner.js";
import Button from "../../Components/Button/Button.js";
import apiURL from "../../config.js";
import './FullPost.css';

const FullPost = () => {

    const params = useParams();
    const dispatch = useDispatch();
    const { currentNews, comments, isLoading } = useSelector(state => state.news, shallowEqual);
    let postImage = noAvailable;
    if(currentNews.image) postImage = `${apiURL}/uploads/${currentNews.image}`;

    const [checkText, setCheckText] = useState(false)
    const [state, setState] = useState({
        author: '',
        comment: '',
    });

    useEffect( () => {
        dispatch(fetchCurrentNews(params.id));
        dispatch(fetchComments(params.id));
    }, [dispatch, params.id])


    useEffect( () => {
        if(state.author.trim() === "" || state.comment.trim() === ""){
            setCheckText(true)
        }else{
            setCheckText(false)
   }
   }, [state.author, state.comment])


    const delteCommentHandler = (id) => {
        dispatch(deleteComment(id))
    }
    
    const createCommentHandler = (e) => {
        e.preventDefault();
        const comment = {
            news: currentNews._id,
            author: state.author,
            comment: state.comment
        }
        dispatch(createComments(comment));
        setState({author:'', comment: ''})
    }

    const inputChangeHandler = e => {
        const {name, value} = e.target;
        setState(prevState => ({...prevState, [name]: value}));
    }

    return (
        <>
            <div className="full_post">
                <div className="full_post_content">
                    <h1>{currentNews.title}</h1>                
                    <p className="full_post_date">
                        <span>Дата публикации: </span>
                        {new Date(currentNews.date).toLocaleString()}               
                    </p>
                    <img className="fullPostImg" src={postImage} alt="" />
                    <p>{currentNews.discription}</p>
                    <hr/>
                    {
                        comments.length > 0 ?
                            <Comments
                                comments={comments}
                                deletehandler={delteCommentHandler}
                            />
                        : 
                        <div>
                            <h1>This post has no comments yet</h1>
                        </div>
                    }
                </div>
                <div className="comment_form">
                    <h3 className="comment_form_title" >Text some comment...</h3>
                    <form onSubmit={createCommentHandler}>
                        <InputTitle 
                            inputName="author"
                            titleChanger={inputChangeHandler} 
                            title={state.author}
                            textPlaceHolder="Введите имя..."
                        />
                        <UserText 
                            textName="comment"
                            textChange={inputChangeHandler} 
                            textValue={state.comment} 
                            textPlaceHolder="Введите комментарий..."
                        />
                        <Button
                            text="Add comment"
                            isDisabled={checkText}
                            create={createCommentHandler}
                        />
                    </form>
                </div>
            </div>
            {isLoading ? <Spinner/> : null}
        </>
    )
}
export default FullPost;